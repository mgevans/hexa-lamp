$fa=1;
$fs=0.25;

module regular_polygon(order, r=1){
    angles=[ for (i = [0:order-1]) i*(360/order) ];
    coords=[ for (th=angles) [r*cos(th), r*sin(th)] ];
    polygon(coords);
}

module screw_pair(r, w) {
    translation_cw = [-w/2, 0];
    translation_ccw = [w/2, 0];

    translate(translation_cw) {
        circle(r=r);
    }

    translate(translation_ccw) {
        circle(r=r);
    }
}


module body(w, h) {
    linear_extrude(h) {
        regular_polygon(6, w/(sqrt(3)));
    }
}

module body_negative(w, h, screw_r, screw_w, body_cavity_d) {
    translate([0, 0, -h/3]) {
        linear_extrude(h*2) {
            // corner holes
            for (i = [0:6]) {
                rotation = [0, 0, (360/6)*i+30];
                rotate(rotation) {
                    translate([0, w/2+3]) {
                        circle(4);
                    }
                }
            }

            // cavity for lamp lip
            circle(d=body_cavity_d);

            translation = [0, w/2];

            for (i = [0:5]) {
                rotation = [0, 0, (360/6)*i];
                rotate(rotation) {
                    translate(translation) {
                        screw_pair(screw_r, screw_w);
                    }
                }
            }
        }
    }
}

module tongues(w, h, tongue_w, tongue_d, tongue_h) {
    translation = [
        -tongue_w/2,
         (w/2)-tongue_d,
        -(h/2)
    ];

    dimensions = [
        tongue_w,
        tongue_d*2,
        tongue_h
    ];

    for (side = [0:5]) {
        rotation = [0, 0, (360/6)*side];
        rotate(rotation) {
            translate(translation) {
                cube(dimensions);
            }
        }
    }
}

module grooves(w, h, groove_w, groove_d, groove_h) {
    translation = [
        -groove_w/2,
         (w/2)-groove_d,
        -(h/2)+groove_h
    ];

    dimensions = [
        groove_w,
        groove_d*2,
        groove_h*2
    ];

    for (side = [0:5]) {
        rotation = [0, 180*(side%2), (360/6)*side];
        rotate(rotation) {
            translate(translation) {
                cube(dimensions);
            }
        }
    }
}

module lamp_cavity(d, h, inner, lip_h) {
    difference() {
        cylinder(d=d, h=h);
        cylinder(d=inner, h=h);
    }

    difference() {
        cylinder(d=d, h=lip_h);
        cylinder(d=inner-10, h=lip_h);
    }

    tab_r = 1.25;
    translation = [0, inner/2, h-tab_r];

    intersection() {
        cylinder(d=d, h=h);

        for (side = [0:5]) {
            rotation = [0, 0, (360/6)*side + (360/12)];
            rotate(rotation) {
                translate(translation) {
                    sphere(r=tab_r);
                }
            }
        }
    }
}

bottom_h = 5.8;
bottom_d = 114.2;
lamp_cavity_d = bottom_d + 1;
body_cavity_d = bottom_d + 3;
screw_r = 1.55;
groove_d = (1.5*3);
lip_h = 1;
body_width = body_cavity_d + 2 * groove_d;

lamp_cavity_h = bottom_h + lip_h;
body_height = lamp_cavity_h;
groove_h = (body_height / 2);
groove_w = 56;
tongue_w = 56;
screw_w = groove_w - (screw_r*2.5*2);
tongue_d = groove_d;
tongue_h = body_height;

module bracket() {
    translation_tg = [0, 0, body_height/2];

    union () {
        difference() {
            union () {
                body(body_width, body_height);
                translate(translation_tg) {
                    tongues(body_width, body_height, tongue_w, tongue_d, tongue_h);
                }
            }

            union () {
                body_negative(body_width, body_height, screw_r, screw_w, body_cavity_d);
                translate(translation_tg) {
                    grooves(body_width, body_height, groove_w, groove_d, groove_h);
                }
            }
        }

        lamp_cavity(body_cavity_d, lamp_cavity_h, lamp_cavity_d, lip_h);
    }
}

module brace() {
    offset = body_width / 2;
    R = body_width / sqrt(3);

    difference() {
        linear_extrude(1) {
            regular_polygon(3, R);
        }

        union() {
            translate([-R/2, offset, 0]) {
                body_negative(body_width, body_height, screw_r, screw_w, body_cavity_d);
            }

            translate([-R/2, -offset, 0]) {
                body_negative(body_width, body_height, screw_r, screw_w, body_cavity_d);
            }

            translate([R, 0, 0]) {
                body_negative(body_width, body_height, screw_r, screw_w, body_cavity_d);
            }
        }
    }
}

//brace();
bracket();

// fin volume
//fins_bot_d = 119;
//fins_top_d = 82.5;
//%cylinder(h=30, d=fins_bot_d);
// bottom volume
//%cylinder(h=bottom_h, d=bottom_d);
